import { Request, Response, NextFunction } from 'express';
import HttpException from '../exceptions/HttpException';
 
function errorMiddleware(error: HttpException, request: Request, response: Response, next: NextFunction) {
    console.log('ERROR', error);
    const status = error.status || 500;
    const message = error.message || 'Something went wrong';
    response.status(status).json({ message: message});
}
 
export default errorMiddleware;