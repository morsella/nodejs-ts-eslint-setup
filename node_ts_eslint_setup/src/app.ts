import express from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import compression from 'compression';
import bcrypt from 'bcryptjs';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import errorMiddleware from './middlewares/ErrorMiddleware';
import headersMiddleware from './middlewares/HeadersMiddleware';
import Controller from './interfaces/ControllerInterface';

class App { 
  app: express.Application;
  constructor(controllers: Controller[]) {
    this.app = express();
 
    // this.connectToTheDatabase();
    this.initMiddlewares();
    this.initHeaders();
    this.initControllers(controllers);
    this.initErrorHandler();
  }

  listen() {
    let port  = process.env.PORT;
    // console.log(process.env);
    this.app.listen(port, () => {
      console.log(`App listening on the port ${port}`);
    });
  }
  initMiddlewares() {
    this.app.use(helmet());
    this.app.use(compression());
    this.app.use(bodyParser.urlencoded({ extended: false })); // x-www-form-urlencoded
    this.app.use(bodyParser.json()) // application/json
  }
  initHeaders(){
    this.app.use(headersMiddleware);
  }
 
  initControllers(controllers: Controller[]) {
    controllers.forEach((controller) => {
      this.app.use('/', controller.router);
    });
  }
  initErrorHandler() {
    this.app.use(errorMiddleware);
  }
  // private connectToTheDatabase() {
  //   const {
  //     MONGO_USER,
  //     MONGO_PASSWORD,
  //     MONGO_PATH,
  //   } = process.env;
  //   mongoose.connect(`mongodb://${MONGO_USER}:${MONGO_PASSWORD}${MONGO_PATH}`);
  // }
}
 
export default App;


