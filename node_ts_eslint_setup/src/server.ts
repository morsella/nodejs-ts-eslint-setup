import dotenv from 'dotenv';
import App from './app';
import BaseRoute from './routes/baseRoute';
dotenv.config({path: './src/.env'});
const app = new App([
    new BaseRoute()
]);

app.listen();