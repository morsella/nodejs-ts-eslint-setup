import { Router, Request, Response } from 'express';
import Controller from "../interfaces/ControllerInterface";

class BaseRoute implements Controller{
    public path = '/';
    public router = Router();
    constructor(){
        this.initRoute();
    }
    private initRoute(){
        this.router.get(this.path, this.redirectBase)
    }
    private redirectBase = async(req: Request, res: Response) => {
        // res.redirect('http://localhost:3000');
        console.log('env', process.env.PORT);
        res.status(200).json({
            message: 'fetched',
        });
    }
}

export default BaseRoute;